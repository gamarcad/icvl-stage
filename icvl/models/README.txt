Contient tous les modèles pré-entraînés.

.
├── embeddings: Modèles pour les embeddings
│   └── frwiki: https://wikipedia2vec.github.io/wikipedia2vec/pretrained/
│       ├── frwiki_20180420_100d.txt.bz2: Embeddings avec une représentation dense de dimension 100
│       └── frwiki_20180420_300d.txt.bz2: Embeddings avec une représentation dense de dimension 300
└── README.txt