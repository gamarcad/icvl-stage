############################
# File: wapiti_data_importer.py
# Author: Gael Marcadet
# Description: Exports data into wapiti readable training/testing files
############################
from icvl.utils.v2.core import load_data_as_words, TokenEncoder, LabelEncoder, _load_snt_file, Dataset, is_punct
from icvl.utils.v2.user_interface import create_parser_to_choose_dataset

WAPITI_FOLDER = "wapiti"
WAPITI_TEST_FILE = "test.snt"
WAPITI_TRAIN_FILE = "train.snt"


def _write_dataset( dataset : Dataset, train_filename, test_filename ):
	tokens_encoder, labels_encoder = dataset.tokens_encoder, dataset.labels_encoder
	X_train, y_train = dataset.train
	print( "There are", len( X_train ), "words and", len(y_train), "labels in train set" )
	with open( train_filename, 'w' ) as file:
		for e_w, e_l in zip( X_train, y_train ):
			w = tokens_encoder.invert_transform_single_token( e_w )
			l = labels_encoder.invert_transform_single_token( e_l )
			file.write( f"{w} {l}\n" + ( "\n" if is_punct( w ) and l == "o"  else "" ) )

	X_test, y_test = dataset.test
	print( "There are", len( X_test ), "words and", len(y_test), "labels in test set" )
	with open( test_filename, 'w' ) as file:
		for e_w, e_l in zip( X_test, y_test ):
			w = tokens_encoder.invert_transform_single_token( e_w )
			l = labels_encoder.invert_transform_single_token( e_l )
			file.write( f"{w} {l}\n" + ( "\n" if is_punct( w ) and l == "o"  else "" ) )
	

def import_data_in_wapiti_folder( directory, files_extension ):
	''' Import data in wapiti folder. '''
	dataset = load_data_as_words( directory, files_extension=files_extension )
	dataset.summary()

	_write_dataset( dataset, train_filename=WAPITI_TRAIN_FILE, test_filename=WAPITI_TEST_FILE )

if __name__ == "__main__":
	# parse arguments
	parser = create_parser_to_choose_dataset()
	args = parser.parse_args()
	
	# import data in wapiti
	import_data_in_wapiti_folder( args.directory, args.extension )
