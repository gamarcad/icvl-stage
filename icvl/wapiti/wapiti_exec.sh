###################################
# File: wapiti_exec.sh
# Author: Gael Marcadet
# Description: Wapiti executable
# 	used to train a model and use
# 	it to predict labels
###################################

MODEL_FILE=model.txt
RESULT_FILE=result.txt
TRAIN_FILE=train.snt
TEST_FILE=test.snt

DATASET="../../data/IOBCorpus"
EXTENSION="snt"


# The first step is to generate train and test data
# This steop produces two files called train.snt and test.snt respectively 
# for the training and testing step
#python3 preformat.py 
python3 wapiti_data_importer.py \
	--directory $DATASET \
	--extension $EXTENSION

# Then we're able to train the model
# train.txt file is formatted as describe in pattern.txt file
# This command generates model.txt file which contains the trained model.
# Available optimizers: bcd, sgd-l1, l-bgfs, rprop(rprop-/rprop+)
echo "============= Training ================="
wapiti train \
	--pattern pattern.txt train.snt \
	--algo rprop- \
	$MODEL_FILE

# Finally we are ready to evaluate the CRF model
echo "============= Testing ================="
wapiti label \
	--model $MODEL_FILE test.snt result.txt\
	--score \
	--post \
	--check

