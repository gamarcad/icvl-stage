################################
# file: metrics.py
# author: Gael Marcadet
# description: Some metrics functions
################################

from icvl.utils.v2.core import flatten

def _secure_computes( calcul ):
	try:
		return calcul()
	except:
		return None

def create_confusion_matrix( y_true, y_pred, n_class = None ):
	y_true, y_pred = flatten( y_true ), flatten( y_pred )
	if len( y_pred ) != len( y_true ):
		raise ValueError( "Prediction and true values haven't the same length" )
	
	# if number of classes, compute it as the max class
	if n_class is None:
		n_class = max( max( y_true ), max( y_pred ) ) + 1

	confusion_matrix = [ [ 0 for _ in range( n_class ) ] for _ in range( n_class ) ]
	for pred_val, real_val in zip( y_pred, y_true ):
		confusion_matrix[ pred_val ][ real_val ] += 1

	return confusion_matrix


def accuracy_by_label( confusion_matrix ):
	label_accuracy = {
		label: _secure_computes( lambda: confusion_matrix[label][label] / sum(confusion_matrix[label]) )
		for label in range( len(confusion_matrix) )
	}
	return label_accuracy


def recall_by_label( confusion_matrix ):
	label_recall = {
		label: _secure_computes( lambda: confusion_matrix[label][label] / sum([ confusion_matrix[i][label] for i in range(len(confusion_matrix)) ]))
		for label in range( len(confusion_matrix) )
	}
	return label_recall

def f1_score_by_label( confusion_matrix ):
	accuracy = accuracy_by_label( confusion_matrix )
	recall = recall_by_label( confusion_matrix )
	f_mesure = {
		label: _secure_computes( lambda: 2 * accuracy[label] * recall[label] / (accuracy[label] + recall[label]) )
		for label, _ in accuracy.items()
	}
	return f_mesure

def summary_by_label( y_true, y_pred, n_class = None ):
	confusion_matrix = create_confusion_matrix( y_true, y_pred, n_class )
	accuracy = accuracy_by_label( confusion_matrix )
	recall = recall_by_label( confusion_matrix )
	f1_score = f1_score_by_label( confusion_matrix )

	return [ ( data[0], data[1:] ) for data in zip( accuracy.keys(), accuracy.values(), recall.values(), f1_score.values() ) ]
		
	

if __name__ == "__main__":
	y_true = [1, 1, 1, 1, 1]
	y_pred = [1, 0, 0, 0, 0]
	for label, (acc, rec, f1) in summary_by_label( y_true, y_pred ):
		print( f"- {label}: Acc = {acc}, \tRec = {rec}, \tF1 = {f1}" )