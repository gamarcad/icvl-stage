##############################
# file: user_interface.py
# author: Gael Marcadet
# description: user interface utilities
##############################

import argparse

def create_parser_to_choose_dataset():
	''' Creates and returns an argument parser to select a dataset. '''
	parser = argparse.ArgumentParser()
	parser.add_argument( "-d", "--directory", help="Specify the corpus", dest="directory", required=True )
	parser.add_argument( "-e", "--extension", help="Specify the extension", dest="extension", required=True )
	parser.add_argument( "-n", "--normalize", action="store_true", help="Normalize input data", dest="normalize" )
	return parser
