--------------------------------------------------
* # Observations:  209397
        # train observations:    140295
        # test observations:     69102
        Train ratio:     0.6699952721385694
        # Classes: 4
                - o
                - i-pers
                - i-org
                - i-lieu
* Shapes
        Input shape:     (140295,)
        Label shape:     (140295,)
* Samples
        Input sample:    [938 437]
        Label sample:  [0 0]
* Labels Repartition
        * Train
                - o: Frequency = 133788,        Repartition = 0.9536191596279269
                - i-lieu: Frequency = 2323,     Repartition = 0.01655796714066788
                - i-org: Frequency = 908,       Repartition = 0.00647207669553441
                - i-pers: Frequency = 3276,     Repartition = 0.023350796535870844
        * Test
                - o: Frequency = 65829,         Repartition = 0.9526352348701919
                - i-lieu: Frequency = 1195,     Repartition = 0.017293276605597523
                - i-pers: Frequency = 1648,     Repartition = 0.023848803218430727
                - i-org: Frequency = 430,       Repartition = 0.006222685305779862
--------------------------------------------------
* Result for GaussianNB model
        * Global metrics
                - Accuracy:      0.9268472692541461
                - Recall:        0.9268472692541461
                - F-Mesure:      0.9268472692541461
        * Metrics by label
                - o: Accuracy = 0.9570258594828104, Recall = 0.9692232906469793, F1 = 0.9630859566631697
                - i-pers: Accuracy = 0.10024650780608052, Recall = 0.14805825242718446, F1 = 0.11954924056834884
                - i-org: Accuracy = None, Recall = 0.0, F1 = None
                - i-lieu: Accuracy = None, Recall = 0.0, F1 = None
------------------------------
* Result for BernoulliNB model
        * Global metrics
                - Accuracy:      0.9526352348701919
                - Recall:        0.9526352348701919
                - F-Mesure:      0.9526352348701919
        * Metrics by label
                - o: Accuracy = 0.9526352348701919, Recall = 1.0, F1 = 0.9757431576138915
                - i-pers: Accuracy = None, Recall = 0.0, F1 = None
                - i-org: Accuracy = None, Recall = 0.0, F1 = None
                - i-lieu: Accuracy = None, Recall = 0.0, F1 = None
------------------------------
* Result for ComplementNB model
        * Global metrics
                - Accuracy:      0.9526352348701919
                - Recall:        0.9526352348701919
                - F-Mesure:      0.9526352348701919
        * Metrics by label
                - o: Accuracy = 0.9526352348701919, Recall = 1.0, F1 = 0.9757431576138915
                - i-pers: Accuracy = None, Recall = 0.0, F1 = None
                - i-org: Accuracy = None, Recall = 0.0, F1 = None
                - i-lieu: Accuracy = None, Recall = 0.0, F1 = None
------------------------------
* Result for MultinomialNB model
        * Global metrics
                - Accuracy:      0.9526352348701919
                - Recall:        0.9526352348701919
                - F-Mesure:      0.9526352348701919
        * Metrics by label
                - o: Accuracy = 0.9526352348701919, Recall = 1.0, F1 = 0.9757431576138915
                - i-pers: Accuracy = None, Recall = 0.0, F1 = None
                - i-org: Accuracy = None, Recall = 0.0, F1 = None
                - i-lieu: Accuracy = None, Recall = 0.0, F1 = None
------------------------------
* Result for CategoricalNB model
        * Global metrics
                - Accuracy:      0.9538653005701716
                - Recall:        0.9538653005701716
                - F-Mesure:      0.9538653005701716
        * Metrics by label
                - o: Accuracy = 0.953915047171862, Recall = 0.9999088547600602, F1 = 0.9763705945176219
                - i-pers: Accuracy = 0.9428571428571428, Recall = 0.04004854368932039, F1 = 0.07683352735739232
                - i-org: Accuracy = None, Recall = 0.0, F1 = None
                - i-lieu: Accuracy = 0.8620689655172413, Recall = 0.02092050209205021, F1 = 0.04084967320261437
------------------------------